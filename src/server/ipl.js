const getMatchesPlayedPerYear = require('../numberOfMatchesPlayedPerYear');
const getWinsPerTeamPerYear = require('../winsPerTeamPerYear');
const getExtraRunsPerTeam = require('../extraRunsPerTeam');
const getTopTenEconomicalBowlers = require('../topTenEconomicalBowlers');

getMatchesPlayedPerYear();
getWinsPerTeamPerYear();
getExtraRunsPerTeam();
getTopTenEconomicalBowlers();
