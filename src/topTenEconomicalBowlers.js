/* eslint-disable camelcase */
const path = require('path');
const iplWrapper = require('./util/readIplData');
const saveAsJson = require('./util/saveAsJson');

const matchesPath = path.resolve(__dirname, './data/matches.csv');
const deliveriesPath = path.resolve(__dirname, './data/deliveries.csv');
const readIplData = iplWrapper();

const topTenEconomicalBowlers = (inputData) => {
  if (inputData) {
    const filePath = path.resolve(__dirname, './public/output/topTenEconomicalBowlers.json');
    const result = inputData.filter((row) => row.season === '2015');
    const matchIds = result.map((row) => row.id);
    readIplData(deliveriesPath, (deliveriesData) => {
      const deliveries = deliveriesData.filter((delivery) => matchIds.includes(delivery.match_id));
      const bowlers = {};
      deliveries.forEach((delivery) => {
        const { bowler, total_runs } = delivery;
        if (!(bowler in bowlers)) {
          bowlers[bowler] = { balls: 1, runs: parseInt(total_runs, 10) };
        } else {
          bowlers[bowler].balls += 1;
          bowlers[bowler].runs += parseInt(total_runs, 10);
        }
      });
      const bowlerKeys = Object.keys(bowlers);
      const economyArray = bowlerKeys.map((key) => {
        const economy = bowlers[key].runs / bowlers[key].balls;
        return ({ bowler: key, economy });
      });
      economyArray.sort((a, b) => a.economy - b.economy);
      const filteredEconomyArray = economyArray.slice(0, 10);
      saveAsJson(filteredEconomyArray, filePath);
    });
  }
};

const getTopTenEconomicalBowlers = () => {
  readIplData(matchesPath, topTenEconomicalBowlers);
};

module.exports = getTopTenEconomicalBowlers;
