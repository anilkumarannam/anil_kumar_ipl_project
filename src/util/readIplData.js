const fs = require('fs');
const { parse } = require('fast-csv');

const iplWrapper = () => {
  const iplData = {};
  const readIplData = (filePath, callBackFunction) => {
    if (filePath) {
      if (!(filePath in iplData)) {
        const rows = [];
        const readStream = fs.createReadStream(filePath);
        readStream.pipe(parse({ headers: true }))
          .on('error', () => { })
          .on('data', (row) => {
            rows.push(row);
          }).on('end', () => {
            iplData[filePath] = rows;
            callBackFunction(rows);
          });
      }
    }
    return iplData[filePath];
  };
  return readIplData;
};

module.exports = iplWrapper;
