const fs = require('fs');

const saveAsJson = (inputData, filePath) => {
  if (inputData) {
    const resultJsonString = JSON.stringify(inputData);
    fs.writeFile(filePath, resultJsonString, (error) => {
      if (error) { throw error; }
    });
  }
};
module.exports = saveAsJson;
