/* eslint-disable camelcase */
const path = require('path');
const iplWrapper = require('./util/readIplData');
const saveAsJson = require('./util/saveAsJson');

const matchesPath = path.resolve(__dirname, './data/matches.csv');
const deliveriesPath = path.resolve(__dirname, './data/deliveries.csv');
const loadIplData = iplWrapper();

const extraRunsPerTeamIn2016 = (inputData) => {
  if (inputData) {
    const filePath = path.resolve(__dirname, './public/output/extraRunsPerTeam2016.json');
    const result = inputData.filter((row) => row.season === '2016');
    const matchIds = result.map((row) => row.id);
    loadIplData(deliveriesPath, (deliveriesData) => {
      const deliveries = deliveriesData.filter((delivery) => matchIds.includes(delivery.match_id));
      const filteredDeliveries = deliveries.filter((delivery) => delivery.extra_runs > 0);
      const extrasPerTeam = {};
      filteredDeliveries.forEach((delivery) => {
        const { batting_team, extra_runs } = delivery;
        if (batting_team in extrasPerTeam) {
          extrasPerTeam[batting_team] += parseInt(extra_runs, 10);
        } else {
          extrasPerTeam[batting_team] = parseInt(extra_runs, 10);
        }
      });
      saveAsJson(extrasPerTeam, filePath);
    });
  }
};

const getExtrasPerTeamIn2016 = () => {
  loadIplData(matchesPath, extraRunsPerTeamIn2016);
};

module.exports = getExtrasPerTeamIn2016;
