const path = require('path');
const iplWrapper = require('./util/readIplData');
const saveAsJson = require('./util/saveAsJson');

const matchesPlayedPerYear = (inputData) => {
  if (inputData) {
    const result = {};
    const filePath = path.resolve(__dirname, './public/output/matchesPlayedPerYear.json');
    inputData.forEach((row) => {
      const { season } = row;
      if (season in result) {
        result[season] += 1;
      } else {
        result[season] = 1;
      }
    });
    saveAsJson(result, filePath);
  }
};

const getMatchesPerTeamPerYear = () => {
  const matchesPath = path.resolve(__dirname, './data/matches.csv');
  const readIplData = iplWrapper();
  readIplData(matchesPath, matchesPlayedPerYear);
};

module.exports = getMatchesPerTeamPerYear;
