const path = require('path');
const iplWrapper = require('./util/readIplData');
const saveAsJson = require('./util/saveAsJson');

const winsPerTeamPerYear = (inputData) => {
  if (inputData) {
    const outputResult = {};
    const filePath = path.resolve(__dirname, './public/output/winsPerTeamPerYear.json');
    inputData.forEach((row) => {
      const { season } = row;
      if (season in outputResult) {
        const seasonObject = outputResult[season];
        const { winner } = row;
        if (winner in seasonObject) {
          seasonObject[winner] += 1;
        } else {
          seasonObject[winner] = 1;
        }
      } else {
        outputResult[season] = {};
      }
    });
    saveAsJson(outputResult, filePath);
  }
};
const getWinsPerTeamPerYear = () => {
  const matchesPath = path.resolve(__dirname, './data/matches.csv');
  const readIplData = iplWrapper();
  readIplData(matchesPath, winsPerTeamPerYear);
};

module.exports = getWinsPerTeamPerYear;
